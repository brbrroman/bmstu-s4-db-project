Rails.application.routes.draw do
  resources :service_events
  resources :problem_events
  get '/members/problem' => 'members#problem'
  get '/members/stat' => 'members#stat'
  get '/elevators/stat' => 'elevators#stat'
  get '/elevators/problem' => 'elevators#problem'
  get '/elevators/service' => 'elevators#service'
  resources :elevators
  resources :members
  resources :member_types
  resources :elevator_types
  resources :houses
  resources :management_companies
  get    '/login'     => 'sessions#new'
  post   '/login'     => 'sessions#create'
  # Выход
  get    '/logout'    => 'sessions#destroy'
  root 'sessions#new'
end
