# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180521064052) do

  create_table "elevator_types", force: :cascade do |t|
    t.string "name"
    t.integer "load_carrying_capacity"
    t.float "nominal_speed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "factory_manufacturer"
  end

  create_table "elevators", force: :cascade do |t|
    t.integer "house_id"
    t.integer "elevator_type_id"
    t.integer "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "serial_number"
    t.datetime "commissioning_date"
    t.index ["elevator_type_id"], name: "index_elevators_on_elevator_type_id"
    t.index ["house_id"], name: "index_elevators_on_house_id"
    t.index ["member_id"], name: "index_elevators_on_member_id"
  end

  create_table "houses", force: :cascade do |t|
    t.string "address"
    t.integer "number_of_storeys"
    t.integer "management_company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["management_company_id"], name: "index_houses_on_management_company_id"
  end

  create_table "management_companies", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "members", force: :cascade do |t|
    t.string "first_name"
    t.string "second_name"
    t.string "phone"
    t.string "login"
    t.string "password_digest"
    t.integer "member_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "patronymic"
    t.index ["member_type_id"], name: "index_members_on_member_type_id"
  end

  create_table "problem_events", force: :cascade do |t|
    t.integer "elevator_id"
    t.boolean "solved"
    t.integer "member_id"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.datetime "planed_date"
    t.index ["elevator_id"], name: "index_problem_events_on_elevator_id"
    t.index ["member_id"], name: "index_problem_events_on_member_id"
  end

  create_table "service_events", force: :cascade do |t|
    t.integer "elevator_id"
    t.string "description"
    t.integer "member_id"
    t.integer "problem_event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.index ["elevator_id"], name: "index_service_events_on_elevator_id"
    t.index ["member_id"], name: "index_service_events_on_member_id"
    t.index ["problem_event_id"], name: "index_service_events_on_problem_event_id"
  end

end
