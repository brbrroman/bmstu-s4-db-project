class CreateElevators < ActiveRecord::Migration[5.1]
  def change
    create_table :elevators do |t|
      t.references :house, foreign_key: true
      t.references :elevator_type, foreign_key: true
      t.references :member, foreign_key: true

      t.timestamps
    end
  end
end
