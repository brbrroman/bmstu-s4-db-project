class CreateMembers < ActiveRecord::Migration[5.1]
  def change
    create_table :members do |t|
      t.string :first_name
      t.string :second_name
      t.string :phone
      t.string :login
      t.string :password
      t.references :member_type, foreign_key: true

      t.timestamps
    end
  end
end
