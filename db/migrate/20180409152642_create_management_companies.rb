class CreateManagementCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :management_companies do |t|
      t.string :name
      t.string :address
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
