class AddPlanedDateToProblemEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :problem_events, :planed_date, :datetime
  end
end
