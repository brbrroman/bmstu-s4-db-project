class CreateProblemEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :problem_events do |t|
      t.references :elevator, foreign_key: true
      t.boolean :solved
      t.references :member, foreign_key: true
      t.string :description

      t.timestamps
    end
  end
end
