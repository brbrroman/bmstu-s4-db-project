class AddFactoryManufacturerToElevatorType < ActiveRecord::Migration[5.1]
  def change
    add_column :elevator_types, :factory_manufacturer, :string
  end
end
