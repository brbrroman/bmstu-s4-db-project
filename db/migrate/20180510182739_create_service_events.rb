class CreateServiceEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :service_events do |t|
      t.references :elevator, foreign_key: true
      t.string :description
      t.references :member, foreign_key: true
      t.references :problem_event, foreign_key: true

      t.timestamps
    end
  end
end
