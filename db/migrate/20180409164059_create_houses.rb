class CreateHouses < ActiveRecord::Migration[5.1]
  def change
    create_table :houses do |t|
      t.string :address
      t.integer :number_of_storeys
      t.references :management_company, foreign_key: true

      t.timestamps
    end
  end
end
