class AddCommissioningDateToElevator < ActiveRecord::Migration[5.1]
  def change
    add_column :elevators, :commissioning_date, :datetime
  end
end
