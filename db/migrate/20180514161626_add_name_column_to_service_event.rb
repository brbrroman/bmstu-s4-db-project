class AddNameColumnToServiceEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :service_events, :name, :string
  end
end
