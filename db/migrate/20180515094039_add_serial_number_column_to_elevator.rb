class AddSerialNumberColumnToElevator < ActiveRecord::Migration[5.1]
  def change
    add_column :elevators, :serial_number, :string
  end
end
