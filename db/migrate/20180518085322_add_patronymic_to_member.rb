class AddPatronymicToMember < ActiveRecord::Migration[5.1]
  def change
    add_column :members, :patronymic, :string
  end
end
