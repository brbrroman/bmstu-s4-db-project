class CreateElevatorTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :elevator_types do |t|
      t.string :name
      t.integer :load_carrying_capacity
      t.float :nominal_speed

      t.timestamps
    end
  end
end
