class ProblemEvent < ApplicationRecord
  belongs_to :elevator
  belongs_to :member
  
  def print_solved
    if solved == false
      'Нет'
    elsif solved.nil? 
      'Nill'
    else
      'Да'
    end
  end
end
