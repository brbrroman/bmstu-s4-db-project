class ServiceEvent < ApplicationRecord
  belongs_to :elevator
  belongs_to :member
  belongs_to :problem_event
end
