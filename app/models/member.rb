class Member < ApplicationRecord
  belongs_to :member_type
  has_secure_password
  def full_name
    "#{second_name} #{first_name} #{patronymic}"
  end
end
