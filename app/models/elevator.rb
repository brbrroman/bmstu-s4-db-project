class Elevator < ApplicationRecord
  belongs_to :house
  belongs_to :elevator_type
  belongs_to :member
end
