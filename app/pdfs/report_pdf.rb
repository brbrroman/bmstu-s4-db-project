class ReportPdf < Prawn::Document
  def initialize(head_name, members, user)
    super()
    font_families.update("OpenSans" => {
      :normal => Rails.root.join("app/assets/fonts/OpenSans-Regular.ttf"),
      :italic => Rails.root.join("app/assets/fonts/OpenSans-Regular.ttf"),
      :bold => Rails.root.join("app/assets/fonts/OpenSans-Regular.ttf"),
      :bold_italic => Rails.root.join("app/assets/fonts/OpenSans-Regular.ttf")
    })
  font "OpenSans"
    @members = members
    puts @members.class
    header(head_name)
    text_content
    table_content
    creation_date = Time.zone.now.strftime("Отчет сгенерирован %e %b %Y в %H:%M")
    go_to_page(page_count)
    move_down(700)
    text creation_date, :align => :right, :style => :italic, :size => 9
    text user.member_type.name + ': ' + user.full_name, :align => :right, :style => :italic, :size => 9
  end

  def header(head_name)
    text "LiftControl", align: :right, style: :italic, :size => 30
    svg IO.read("#{Rails.root}/app/assets/images/elevator_black.svg"), width: 30, height: 30
    text head_name.to_s, size: 16, style: :bold
  end

  def text_content
    if @members.class == Member
      text @members.member_type.name + ': ' + @members.full_name, size: 16
    elsif @members.class == Elevator
      text 'Серийный номер: ' + @members.serial_number, size: 16
    end
  end
  
  def table_content    
    table member_rows do
      row(0).font_style = :bold
      self.header = true
      self.row_colors = ['F2F2F2', 'FFFFFF']
    end
  end

  def member_rows
    if @members.class == Member
      [['Название', 'Время создания', 'Проблема решена?']] +
        ProblemEvent.where(member: @members.id).map do |problem_event|
        [problem_event.name, Time.zone.at(problem_event.created_at).strftime("%F %T"), problem_event.print_solved]
      end
    elsif @members.class == Elevator
          [['Название', 'Время создания', 'Проблема решена?']] +
        ProblemEvent.where(elevator: @members.id).map do |problem_event|
        [problem_event.name, Time.zone.at(problem_event.created_at).strftime("%F %T"), problem_event.print_solved]
      end
    else
      [[""]]
    end
  end
end
