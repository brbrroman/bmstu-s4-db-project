class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def current_user
    @current_user ||= Member.find(session[:user_id]) if session[:user_id]
  rescue
    # На случай утери id пользователя из базы данных
    redirect_to '/logout'
  end

  helper_method :current_user
  helper_method :hr?
  helper_method :elevator_manager?
  helper_method :operator?
  helper_method :elevator_repair_specialist?

  def hr?
    if current_user
      current_user.member_type_id == 1
    end
  end
  
  def elevator_manager?
    if current_user
      current_user.member_type_id == 2
    end
  end
  
  def operator? # Оператор по приему заявок
    if current_user
      current_user.member_type_id == 3
    end
  end
  
  def elevator_repair_specialist?
    if current_user
      current_user.member_type_id == 4
    end
  end

  def hr
    redirect_to :root unless hr?
  end
  
  def elevator_manager
    redirect_to :root unless elevator_manager?
  end
  
  def operator
    redirect_to :root unless operator?
  end
  
  def elevator_repair_specialist
    redirect_to :root unless elevator_repair_specialist?
  end
  
  # Если пользователь не зайден
  def authorize
    redirect_to :root unless current_user
  end

  # Если пользователь залогинен, то нужно не дать перейти на некоторые страницы
  def deny_access_to_authorized
    redirect_to :members        if hr?
    redirect_to :elevators      if elevator_manager?
    redirect_to :problem_events if operator?
    redirect_to :problem_events if elevator_repair_specialist?
  end
  # нельзя объединять предыдущие 2 метода
end
