class ManagementCompaniesController < ApplicationController
  before_action :authorize
  before_action :set_management_company, only: [:show, :edit, :update, :destroy]

  # GET /management_companies
  # GET /management_companies.json
  def index
    @management_companies = ManagementCompany.all
  end

  # GET /management_companies/1
  # GET /management_companies/1.json
  def show
  end

  # GET /management_companies/new
  def new
    @management_company = ManagementCompany.new
  end

  # GET /management_companies/1/edit
  def edit
  end

  # POST /management_companies
  # POST /management_companies.json
  def create
    @management_company = ManagementCompany.new(management_company_params)

    respond_to do |format|
      if @management_company.save
        format.html { redirect_to @management_company, notice: t('Management company was successfully created') }
        format.json { render :show, status: :created, location: @management_company }
      else
        format.html { render :new }
        format.json { render json: @management_company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /management_companies/1
  # PATCH/PUT /management_companies/1.json
  def update
    respond_to do |format|
      if @management_company.update(management_company_params)
        format.html { redirect_to @management_company, notice: t('Management company was successfully updated') }
        format.json { render :show, status: :ok, location: @management_company }
      else
        format.html { render :edit }
        format.json { render json: @management_company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /management_companies/1
  # DELETE /management_companies/1.json
  def destroy
    @management_company.destroy
    respond_to do |format|
      format.html { redirect_to management_companies_url, notice: t('Management company was successfully destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_management_company
      @management_company = ManagementCompany.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def management_company_params
      params.require(:management_company).permit(:name, :address, :email, :phone)
    end
end
