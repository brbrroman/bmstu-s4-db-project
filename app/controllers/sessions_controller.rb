# app/controllers/sessions_controller.rb
class SessionsController < ApplicationController
  # Не дать залогиненому пользователю перейти на страницу входа
  before_action :deny_access_to_authorized, only: %i[new create]

  def create
    user = Member.find_by_login(params[:login])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to :root
    else
      flash[:alert] = 'Invalid login or password'
      # в случае неудачи нужно частично заполнить форму ввода
      flash[:login] = params[:login]
      redirect_to :root
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to :root
  end
end
