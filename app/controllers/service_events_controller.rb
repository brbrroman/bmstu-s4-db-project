class ServiceEventsController < ApplicationController
  before_action :authorize
  before_action :set_service_event, only: [:show, :edit, :update, :destroy]

  # GET /service_events
  # GET /service_events.json
  def index
    if elevator_repair_specialist?
      @service_events = ServiceEvent.where(elevator_id: Elevator.where(member_id: current_user.id))
    else
      @service_events = ServiceEvent.all
    end
  end

  # GET /service_events/1
  # GET /service_events/1.json
  def show
  end

  # GET /service_events/new
  def new
    @problem_events = ProblemEvent.where("member_id = ? and planed_date <= ? or member_id = ? and planed_date IS ?", current_user.id, Time.now, current_user.id, nil)
    @service_event = ServiceEvent.new
  end

  # GET /service_events/1/edit
  def edit
    @problem_events = ProblemEvent.where("member_id = ? and planed_date <= ? or member_id = ? and planed_date IS ?", current_user.id, Time.now, current_user.id, nil)
  end

  # POST /service_events
  # POST /service_events.json
  def create
    @service_event = ServiceEvent.new(service_event_params)

    respond_to do |format|
      if @service_event.save
        format.html { redirect_to @service_event, notice: t('Service event was successfully created') }
        format.json { render :show, status: :created, location: @service_event }
      else
        format.html { render :new }
        format.json { render json: @service_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /service_events/1
  # PATCH/PUT /service_events/1.json
  def update
    respond_to do |format|
      if @service_event.update(service_event_params)
        ProblemEvent.find_by_id(@service_event.problem_event_id).update(solved: (params[:solved] == 'Yes'))
        format.html { redirect_to @service_event, notice: t('Service event was successfully updated') }
        format.json { render :show, status: :ok, location: @service_event }
      else
        format.html { render :edit }
        format.json { render json: @service_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /service_events/1
  # DELETE /service_events/1.json
  def destroy
    @service_event.destroy
    respond_to do |format|
      format.html { redirect_to service_events_url, notice: t('Service event was successfully destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service_event
      @service_event = ServiceEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_event_params
      params.require(:service_event).merge!(member_id: current_user.id).permit(:elevator_id, :description, :member_id, :problem_event_id, :name)
    end
end
