class ProblemEventsController < ApplicationController
  before_action :authorize
  before_action :set_problem_event, only: [:show, :edit, :update, :destroy]

  # GET /problem_events
  # GET /problem_events.json
  def index
    if elevator_repair_specialist?
      @problem_events = ProblemEvent.where("member_id = ? and planed_date <= ? or member_id = ? and planed_date IS ?", current_user.id, Time.now, current_user.id, nil)
    else
      @problem_events = ProblemEvent.all
    end
  end

  # GET /problem_events/1
  # GET /problem_events/1.json
  def show
  end

  # GET /problem_events/new
  def new
    @problem_event = ProblemEvent.new
  end

  # GET /problem_events/1/edit
  def edit
  end

  # POST /problem_events
  # POST /problem_events.json
  def create
    @problem_event = ProblemEvent.new(problem_event_params)
    @problem_event.solved = false
    respond_to do |format|
      if @problem_event.save
        format.html { redirect_to @problem_event, notice: t('Problem event was successfully created') }
        format.json { render :show, status: :created, location: @problem_event }
      else
        format.html { render :new }
        format.json { render json: @problem_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /problem_events/1
  # PATCH/PUT /problem_events/1.json
  def update
    respond_to do |format|
      if @problem_event.update(problem_event_params)
        format.html { redirect_to @problem_event, notice: t('Problem event was successfully updated') }
        format.json { render :show, status: :ok, location: @problem_event }
      else
        format.html { render :edit }
        format.json { render json: @problem_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /problem_events/1
  # DELETE /problem_events/1.json
  def destroy
    @problem_event.destroy
    respond_to do |format|
      format.html { redirect_to problem_events_url, notice: t('Problem event was successfully destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_problem_event
      @problem_event = ProblemEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def problem_event_params
      params.require(:problem_event).permit(:elevator_id, :solved, :member_id, :description, :name, :planed_date)
    end
end
