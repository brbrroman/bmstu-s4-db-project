class ElevatorTypesController < ApplicationController
  before_action :authorize
  before_action :set_elevator_type, only: [:show, :edit, :update, :destroy]

  # GET /elevator_types
  # GET /elevator_types.json
  def index
    @elevator_types = ElevatorType.all
  end

  # GET /elevator_types/1
  # GET /elevator_types/1.json
  def show
  end

  # GET /elevator_types/new
  def new
    @elevator_type = ElevatorType.new
  end

  # GET /elevator_types/1/edit
  def edit
  end

  # POST /elevator_types
  # POST /elevator_types.json
  def create
    @elevator_type = ElevatorType.new(elevator_type_params)

    respond_to do |format|
      if @elevator_type.save
        format.html { redirect_to @elevator_type, notice: t('Elevator type was successfully created') }
        format.json { render :show, status: :created, location: @elevator_type }
      else
        format.html { render :new }
        format.json { render json: @elevator_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /elevator_types/1
  # PATCH/PUT /elevator_types/1.json
  def update
    respond_to do |format|
      if @elevator_type.update(elevator_type_params)
        format.html { redirect_to @elevator_type, notice: t('Elevator type was successfully updated') }
        format.json { render :show, status: :ok, location: @elevator_type }
      else
        format.html { render :edit }
        format.json { render json: @elevator_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /elevator_types/1
  # DELETE /elevator_types/1.json
  def destroy
    @elevator_type.destroy
    respond_to do |format|
      format.html { redirect_to elevator_types_url, notice: t('Elevator type was successfully destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_elevator_type
      @elevator_type = ElevatorType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def elevator_type_params
      params.require(:elevator_type).permit(:name, :load_carrying_capacity, :nominal_speed, :factory_manufacturer)
    end
end
