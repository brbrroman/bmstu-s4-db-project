class MembersController < ApplicationController
  before_action :authorize
  before_action :set_member, only: [:show, :edit, :update, :destroy]

  # GET /members
  # GET /members.json
  def index
    @members = Member.all
  end

  def stat
    @members = Member.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ReportPdf.new(t('Problem stat')+t('by members'), @members.where(member_type: 4), current_user)
        send_data pdf.render, filename: t('Problem stat')+t('by members')+'.pdf', type: 'application/pdf'
      end
    end
  end
  
  def problem
    @member = Member.find(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ReportPdf.new(t('Problem stat')+t('by members'), @member, current_user)
        send_data pdf.render, filename: t('Problem stat')+'-'+@member.full_name+'.pdf', type: 'application/pdf'
      end
    end
  end
  
  # GET /members/1
  # GET /members/1.json
  def show
    @member = Member.find(params[:id])
  end

  # GET /members/new
  def new
    @member = Member.new
  end

  # GET /members/1/edit
  def edit
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)

    respond_to do |format|
      if @member.save
        format.html { redirect_to @member, notice: t('Member was successfully created') }
        format.json { render :show, status: :created, location: @member }
      else
        format.html { render :new }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    respond_to do |format|
      if @member.update(member_params)
        format.html { redirect_to @member, notice: t('Member was successfully updated') }
        format.json { render :show, status: :ok, location: @member }
      else
        format.html { render :edit }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @member.destroy
    respond_to do |format|
      format.html { redirect_to members_url, notice: t('Member was successfully destroyed') }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
      params.require(:member).permit(:first_name, :second_name, :phone, :login, :password, :member_type_id, :patronymic)
    end
end
