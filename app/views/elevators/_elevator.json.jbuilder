json.extract! elevator, :id, :house_id, :elevator_type_id, :member_id, :created_at, :updated_at
json.url elevator_url(elevator, format: :json)
