json.extract! problem_event, :id, :elevator_id, :solved, :member_id, :description, :created_at, :updated_at
json.url problem_event_url(problem_event, format: :json)
