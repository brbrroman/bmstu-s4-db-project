json.extract! house, :id, :address, :number_of_storeys, :management_company_id, :created_at, :updated_at
json.url house_url(house, format: :json)
