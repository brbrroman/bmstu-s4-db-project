json.extract! management_company, :id, :name, :address, :email, :phone, :created_at, :updated_at
json.url management_company_url(management_company, format: :json)
