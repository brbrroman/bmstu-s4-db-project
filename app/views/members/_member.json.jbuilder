json.extract! member, :id, :first_name, :second_name, :phone, :login, :member_type_id, :created_at, :updated_at
json.url member_url(member, format: :json)
