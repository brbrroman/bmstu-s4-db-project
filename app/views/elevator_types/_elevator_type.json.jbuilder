json.extract! elevator_type, :id, :name, :load_carrying_capacity, :nominal_speed, :created_at, :updated_at
json.url elevator_type_url(elevator_type, format: :json)
