json.extract! service_event, :id, :elevator_id, :description, :member_id, :problem_event_id, :created_at, :updated_at
json.url service_event_url(service_event, format: :json)
