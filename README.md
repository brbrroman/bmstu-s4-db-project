# ППК"Лифтконтроль"МЭ

Полное наименование программной разработки: «Прототип программного комплекса
автоматизации работы службы обслуживания лифтов жилых домов малой этажности
«ЛифтКонтроль»», в дальнейшем именуемой как ППК«Лифтконтроль»МЭ.

ППК«Лифтконтроль»МЭ предназначен для учета сотрудников службы, учета
обслуживаемых лифтов, формирования и хранения перечня регламентных работ, регистрации
заявок о неисправностях и регистрации плановых и внеплановых работ на обслуживаемых
лифтах. ППК«Лифтконтроль»МЭ предназначен для использования аварийно-
диспетчерскими службами, лифтовыми предприятиями и сервисными службами, видом
деятельности которых является обслуживание лифтов в жилых домах малой этажности.
